exports.navidad = () => {
    var Hoy=new Date()
    var Nav=new Date(Hoy.getFullYear(), 11, 25)
    var mseg_dia=1000*60*60*24
    var dias
    
    if (Hoy.getMonth()==11 && Hoy.getDate()>25) 

        Nav.setFullYear(Nav.getFullYear()+1) 

    dias = Math.ceil((Nav.getTime()-Hoy.getTime())/(mseg_dia))

    return "Quedan "+dias+" días hasta Navidad"

}


exports.edad = (day, month, year) => {
    var anoActual = new Date().getFullYear()
    return anoActual - year
}

/* console.log(edad(new Date(2000, 08, 28))); */

function existeFecha (fecha) {
        var fechaf = fecha.split("-");
        var d = fechaf[0];
        var m = fechaf[1];
        var y = fechaf[2];
        return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
}

exports.validar = function(nombre,email,fecha){
    if(nombre == "" || email == "" || fecha == ""){
        return false;
    }
    else{
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
            return false;
        }
        
        if (!existeFecha(fecha)){
            return false
        }
        
        //console.log("Datos validados correctamente");
        return true;
    }

    
}